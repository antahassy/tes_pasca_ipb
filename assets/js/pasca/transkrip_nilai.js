$(document).ready(function(){
	var sum = (index, value) => index + value;
    var arr_matkul = '';
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if($.inArray(e, result) == -1){
                result.push(e);
            }
        });
        return result;
    }
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                o_semester();
            },500);
        }
    });
    function o_semester(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'pasca_log/o_semester',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                var s_semester = '<option value="">Pilih Semester</option>';
                for(i = 0; i < data.length; i ++){
                    s_semester += '<option value="' + data[i].id_semester + '">' + data[i].semester + '</option>';
                }
                $('#s_semester').html(s_semester);
                main_data();
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' +
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    var table_data;
    function main_data(){
        table_data = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            order               : [],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'pasca_log/table_transkrip',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        var modal_form_data;
        $('#modal_form_data').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form_data = true;
        });
        $('#modal_form_data').on('hide.bs.modal', function(){
            if(modal_form_data){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form_data = false;
                setTimeout(function(){
                    $('#modal_form_data').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        var modal_form_nilai;
        $('#modal_form_nilai').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form_nilai = true;
        });
        $('#modal_form_nilai').on('hide.bs.modal', function(){
            if(modal_form_nilai){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form_nilai = false;
                setTimeout(function(){
                    $('#modal_form_nilai').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            $('#modal_form_data').find('.modal-title').text('Transkrip Baru');
            $('#btn_save').text('Simpan');
            $('#form_data')[0].reset();
            $('#form_ip_address').val(ip_address);
            $('#form_data').attr('action', site + 'pasca_log/save_transkrip');
            $('#modal_form_data').modal('show');
        });
        $('#nim_mahasiswa').autocomplete({
            delay   : 1500,
            source  : site + 'pasca_log/autocomplete_nim/?',
            select  : function(event, ui){
                var value = ui.item.value;
                $.ajax({
                    type        : 'ajax',
                    method      : 'post',
                    url         : site + 'pasca_log/nim_complete',
                    data        : {nim : value},
                    async       : true,
                    dataType    : 'json',
                    success     : function(data){
                        $('#nim_mahasiswa').val(data.nim);
                        $('#nm_mahasiswa').val(data.nama);
                    },
                    error       : function(){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Koneksi terputus' + '<br>' +
                                          'Cobalah beberapa saat lagi</pre>',
                            type        : "warning"
                        });
                    }
                });
            }
        });
        $('#nm_mahasiswa').autocomplete({
            delay   : 1500,
            source  : site + 'pasca_log/autocomplete_nama/?',
            select  : function(event, ui){
                var value = ui.item.value;
                $.ajax({
                    type        : 'ajax',
                    method      : 'post',
                    url         : site + 'pasca_log/nama_complete',
                    data        : {nama : value},
                    async       : true,
                    dataType    : 'json',
                    success     : function(data){
                        $('#nim_mahasiswa').val(data.nim);
                        $('#nm_mahasiswa').val(data.nama);
                    },
                    error       : function(){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Koneksi terputus' + '<br>' +
                                          'Cobalah beberapa saat lagi</pre>',
                            type        : "warning"
                        });
                    }
                });
            }
        });
        $('#table_data').on('click', '.btn_edit', function(){
            arr_matkul = '';
            var id_transkrip = $(this).attr('id');
            var id_jurusan = $(this).attr('alt');
            var id_semester = $(this).attr('data');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'pasca_log/transkrip_data',
                            data           : {
                                id_transkrip    : id_transkrip,
                                id_jurusan      : id_jurusan,
                                id_semester     : id_semester
                            },
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                if(data.empty){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Gagal mengambil data mata kuliah' + '<br>' + 
                                                      'Belum ada mata kuliah terdaftar' + '<br>' + 
                                                      'Untuk semester & jurusan mahasiswa ini</pre>',
                                        type        : "warning"
                                    });
                                }else{
                                    arr_matkul = data.matkul;
                                    $('#modal_form_nilai').find('.modal-title').text('Transkrip Nilai');
                                    $('#btn_save_nilai').text('Simpan');
                                    $('#form_nilai')[0].reset();
                                    $('#form_nilai_ip_address').val(ip_address);
                                    $('#transkrip_nim').text(': ' + data.nim);
                                    $('#transkrip_nama').text(': ' + data.nama);
                                    $('#transkrip_semester').text(': ' + data.semester);
                                    $('#transkrip_jurusan').text(': ' + data.jurusan);
                                    transkrip_nilai(arr_matkul, id_transkrip);
                                }
                            },
                            error   : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#btn_save').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url          = $('#form_data').attr('action');
            var form_data  = $('#form_data')[0];
            form_data  = new FormData(form_data);
            var errormessage = '';
            if(! $('#nim_mahasiswa').val()){
                errormessage += 'NIM mahasiswa dibutuhkan \n';
            }
            if(! $('#nm_mahasiswa').val()){
                errormessage += 'Nama mahasiswa dibutuhkan \n';
            }
            if(! $('#s_semester').val()){
                errormessage += 'Semester dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    html        : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                save_data(url, form_data);
            }
            return false;
        });
    }
    function transkrip_nilai(arr_matkul, id_transkrip){
        var arr_transkrip_nilai = [];
        $('#tbody_transkrip_nilai').empty();
        var tbody_transkrip_nilai = '', count_nilai = 0;
        for(i = 0; i < arr_matkul.length; i ++){
            $.ajax({
                type           : 'ajax',
                method         : 'post',
                url            : site + 'pasca_log/transkrip_nilai',
                data           : {
                    id_transkrip    : id_transkrip,
                    id_mata_kuliah  : arr_matkul[i].id_mata_kuliah
                },
                async          : false,
                dataType       : 'json',
                success        : function(data){
                    if(data.empty){
                        tbody_transkrip_nilai += 
                        '<tr>' +
                            '<td>' + arr_matkul[i].kd + '</td>' +
                            '<td>' + arr_matkul[i].nama + '</td>' +
                            '<td><input type="text" class="arr_nilai" alt="' + id_transkrip + '" id="' + arr_matkul[i].id_mata_kuliah + '"></td>' +
                        '</tr>';
                    }else{
                        tbody_transkrip_nilai += 
                        '<tr>' +
                            '<td>' + arr_matkul[i].kd + '</td>' +
                            '<td>' + arr_matkul[i].nama + '</td>' +
                            '<td><input type="text" class="arr_nilai" alt="' + id_transkrip + '" id="' + data.id_mata_kuliah + '" value="' + data.nilai + '"></td>' +
                        '</tr>';
                        arr_transkrip_nilai.push({
                            id_transkrip    : id_transkrip,
                            id_mata_kuliah  : data.id_mata_kuliah,
                            value           : data.nilai
                        });
                    }
                    count_nilai ++;
                    if(count_nilai == arr_matkul.length){
                        $('#tbody_transkrip_nilai').html(tbody_transkrip_nilai);
                        main_transkrip_nilai(arr_transkrip_nilai);
                        swal.close();
                        $('#modal_form_nilai').modal('show');
                    }
                },
                error   : function(){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                      'Cobalah beberapa saat lagi</pre>',
                        type        : "warning"
                    });
                }
            });
        }
    }
    function main_transkrip_nilai(arr_transkrip_nilai){
        $('.arr_nilai').on('keyup', function(){
            if(arr_transkrip_nilai.length == 0){
                arr_transkrip_nilai.push({
                    id_transkrip    : $(this).attr('alt'),
                    id_mata_kuliah  : $(this).attr('id'),
                    value           : $(this).val(),
                });
            }else{
                var filtered = arr_transkrip_nilai.map(a => a.id_mata_kuliah);
                if(filtered.includes($(this).attr('id'))){
                    for(i = 0; i < arr_transkrip_nilai.length; i ++){
                        if(arr_transkrip_nilai[i].id_mata_kuliah == $(this).attr('id')){
                            arr_transkrip_nilai[i].value = $(this).val();
                        }
                    }
                }else{
                    arr_transkrip_nilai.push({
                        id_transkrip    : $(this).attr('alt'),
                        id_mata_kuliah  : $(this).attr('id'),
                        value           : $(this).val(),
                    });
                }
            }
        });
        $('#btn_save_nilai').on('click', function(){
            var errormessage = '';
            if(arr_transkrip_nilai.length == 0){
                errormessage += 'Harap isi salah satu nilai \n';
            }
            if(errormessage !== ''){
                swal({
                    html        : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            var count_transkrip = 0;
                            for(x = 0; x < arr_transkrip_nilai.length; x ++){
                                $.ajax({
                                    type        : 'ajax',
                                    method      : 'post',
                                    url         : site + 'pasca_log/save_transkrip_nilai',
                                    data        : {
                                        id_transkrip    : arr_transkrip_nilai[x].id_transkrip,
                                        id_mata_kuliah  : arr_transkrip_nilai[x].id_mata_kuliah,
                                        nilai           : arr_transkrip_nilai[x].value
                                    },
                                    async       : false,
                                    dataType    : 'json',
                                    success     : function(response){
                                        if(response.success){
                                            count_transkrip ++;
                                            if(count_transkrip == arr_transkrip_nilai.length){
                                                $('#modal_form_nilai').modal('hide');
                                                $('#tbody_transkrip_nilai').empty();
                                                swal.close();
                                                setTimeout(function(){
                                                    $('#table_data').DataTable().ajax.reload();
                                                },500);
                                            }
                                        }
                                    },
                                    error       : function(){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Koneksi terputus' + '<br>' +
                                                          'Cobalah beberapa saat lagi</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            }
                        }, 500);
                    }
                });
            }
            return false;
        });
    }
    function save_data(url, form_data){
        var text_info = '';
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    $.ajax({
                        type        : 'ajax',
                        method      : 'post',
                        url         : url,
                        data        : form_data,
                        async       : true,
                        processData : false,
                        contentType : false,
                        cache       : false,
                        dataType    : 'json',
                        success     : function(response){
                            if(response.success){
                                $('#modal_form_data').modal('hide');
                                $('#form_data')[0].reset();
                                if(response.type == 'saved'){
                                    text_info = 'disimpan';
                                }
                                if(response.type == 'updated'){
                                    text_info = 'diupdate';
                                }
                                swal({
                                    html                : '<pre>Data transkrip berhasil ' + text_info + '</pre>',
                                    type                : "success",
                                    allowOutsideClick   : false,
                                    allowEscapeKey      : false, 
                                    background          : 'transparent',
                                    showConfirmButton   : false,
                                    timer               : 1000
                                }).then(function(){
                                    $('#table_data').DataTable().ajax.reload();
                                });
                            }
                            if(response.empty){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Nim dan nama mahasiswa' + '<br>' +
                                                  'Tidak ada dalam data mahasiswa</pre>',
                                    type        : "warning"
                                });
                            }
                            if(response.match){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Nim dan nama mahasiswa' + '<br>' +
                                                  'Sudah ada dengan data semester sama</pre>',
                                    type        : "warning"
                                });
                            }
                        },
                        error       : function(){
                            swal({
                                background  : 'transparent',
                                html        : '<pre>Koneksi terputus' + '<br>' +
                                              'Cobalah beberapa saat lagi</pre>',
                                type        : "warning"
                            });
                        }
                    });
                }, 500);
            }
        });
    }
});