$(document).ready(function(){
    banner();
    function banner(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'pasca_log/all_banner',
            dataType    : "json",
            async       : true,
            success: function(data){
                if(data.length == 1){
                    $('.bg-image').css('background-image','url(' +  site + 'assets/banner/' + data[0].file + ')');
                }else{
                    var banner_index = 0;
                    $('.bg-image').css('background-image','url(' +  site + 'assets/banner/' + data[banner_index].file + ')');
                    setInterval(function() {
                        $('.bg-image').css('background-image','url(' +  site + 'assets/banner/' + data[banner_index].file + ')').removeClass('slideInDown').addClass('slideOutUp').show(1000);
                        setTimeout(function(){
                            $('.bg-image').css('background-image','url(' +  site + 'assets/banner/' + data[banner_index].file + ')').removeClass('slideOutUp').addClass('slideInDown').show(1000);
                        },1000);
                        data[banner_index++];
                        if(banner_index == data.length){
                            banner_index = 0;
                        }
                    }, 7500);
                }
            },
            error: function (){
                console.log('error getting banner');
            }
        });
    }
});