$(document).ready(function(){
	var sum = (index, value) => index + value;
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if($.inArray(e, result) == -1){
                result.push(e);
            }
        });
        return result;
    }
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                o_semester();
            },500);
        }
    });
    function o_semester(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'pasca_log/o_semester',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                var s_semester = '<option value="">Pilih Semester</option>';
                for(i = 0; i < data.length; i ++){
                    s_semester += '<option value="' + data[i].id_semester + '">' + data[i].semester + '</option>';
                }
                $('#s_semester').html(s_semester);
                o_jurusan();
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' +
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function o_jurusan(){
        $.ajax({
            type        : 'ajax',
            method      : 'get',
            url         : site + 'pasca_log/o_jurusan',
            async       : true,
            dataType    : 'json',
            success     : function(data){
                var s_jurusan = '<option value="">Pilih Jurusan</option>';
                for(i = 0; i < data.length; i ++){
                    s_jurusan += '<option value="' + data[i].id_jurusan + '">' + data[i].jurusan + '</option>';
                }
                $('#s_jurusan').html(s_jurusan);
                main_data();
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' +
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    var table_data;
    function main_data(){
        table_data = $('#table_data').DataTable({ 
            processing          : true, 
            destroy             : true,
            serverSide          : true, 
            scrollX             : true,
            scrollCollapse      : true,
            fixedColumns        : true, 
            order               : [],
            initComplete: function(){
                swal.close();
            },
            ajax            : {
                url         : site + 'pasca_log/table_mata_kuliah',
                method      : 'post'
            },
            columnDefs: [{ 
                targets   : [0], 
                orderable : false 
            }]
        });
    }
    main();
    function main(){
        var modal_form_data;
        $('#modal_form_data').on('show.bs.modal', function(){
            $(this).addClass('zoomIn');
            modal_form_data = true;
        });
        $('#modal_form_data').on('hide.bs.modal', function(){
            if(modal_form_data){
                $(this).removeClass('zoomIn').addClass('zoomOut');
                modal_form_data = false;
                setTimeout(function(){
                    $('#modal_form_data').modal('hide');
                },500);
                return false;
            }
            $(this).removeClass('zoomOut');
        });
        $('#btn_add').on('click', function(){
            $('#modal_form_data').find('.modal-title').text('Tambah Mata Kuliah');
            $('#btn_save').text('Simpan');
            $('#form_data')[0].reset();
            $('#form_ip_address').val(ip_address);
            $('#form_data').attr('action', site + 'pasca_log/save_matkul');
            $('#modal_form_data').modal('show');
        });
        $('#table_data').on('click', '.btn_edit', function(){
            var id_data = $(this).attr('id');
            swal({
                showConfirmButton   : false,
                allowOutsideClick   : false,
                allowEscapeKey      : false,
                background          : 'transparent',
                onOpen  : function(){
                    swal.showLoading();
                    setTimeout(function(){
                        $.ajax({
                            type           : 'ajax',
                            method         : 'post',
                            url            : site + 'pasca_log/edit_matkul',
                            data           : {
                                id_mata_kuliah  : id_data
                            },
                            async          : true,
                            dataType       : 'json',
                            success        : function(data){
                                $('#form_ip_address').val(ip_address);
                                $('#id_mata_kuliah').val(data.id_mata_kuliah);
                                $('#kd_matkul').val(data.kd);
                                $('#nm_matkul').val(data.nama);
                                $('#s_semester').val(data.id_semester);
                                $('#s_jurusan').val(data.id_jurusan);
                                $('#modal_form_data').find('.modal-title').text('Edit Mata Kuliah');
                                $('#btn_save').text('Update');
                                $('#form_data').attr('action', site + 'pasca_log/update_matkul');
                                swal.close();
                                $('#modal_form_data').modal('show');
                            },
                            error   : function(){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                                  'Cobalah beberapa saat lagi</pre>',
                                    type        : "warning"
                                });
                            }
                        });
                    },500);
                }
            });
        });
        $('#table_data').on('click', '.btn_delete', function(){
            var id_data = $(this).attr('id');
            var matkul = $(this).attr('data');
            swal({
                background          : 'transparent',
                html                : '<pre>Hapus mata kuliah ' + matkul + ' ?</pre>',
                type                : 'question',
                showCancelButton    : true,
                cancelButtonText    : 'Tidak',
                confirmButtonText   : 'Ya'
            }).then((result) => {
                if(result.value){
                    swal({
                        showConfirmButton   : false,
                        allowOutsideClick   : false,
                        allowEscapeKey      : false,
                        background          : 'transparent',
                        onOpen  : function(){
                            swal.showLoading();
                            setTimeout(function(){
                                $.ajax({
                                    type           : 'ajax',
                                    method         : 'post',
                                    url            : site + 'pasca_log/delete_matkul',
                                    data           : {
                                        id_mata_kuliah  : id_data
                                    },
                                    async          : true,
                                    dataType       : 'json',
                                    success        : function(response){
                                        if(response.success){
                                            $('#modal_form_data').modal('hide');
                                            swal({
                                                html                : '<pre>Data mata kuliah berhasil dihapus</pre>',
                                                type                : "success",
                                                allowOutsideClick   : false,
                                                allowEscapeKey      : false, 
                                                background          : 'transparent',
                                                showConfirmButton   : false,
                                                timer               : 1000
                                            }).then(function(){
                                                $('#table_data').DataTable().ajax.reload();
                                            });
                                        }
                                    },
                                    error   : function(){
                                        swal({
                                            background  : 'transparent',
                                            html        : '<pre>Koneksi terputus' + '<br>' + 
                                                          'Cobalah beberapa saat lagi</pre>',
                                            type        : "warning"
                                        });
                                    }
                                });
                            },500);
                        }
                    });
                }
            });
        });
        $('#btn_save').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var url          = $('#form_data').attr('action');
            var form_data  = $('#form_data')[0];
            form_data  = new FormData(form_data);
            var errormessage = '';
            if(! $('#kd_matkul').val()){
                errormessage += 'Kode mata kuliah dibutuhkan \n';
            }
            if(! $('#nm_matkul').val()){
                errormessage += 'Nama mata kuliah dibutuhkan \n';
            }
            if(! $('#s_semester').val()){
                errormessage += 'Semester dibutuhkan \n';
            }
            if(! $('#s_jurusan').val()){
                errormessage += 'Jurusan dibutuhkan \n';
            }
            if(errormessage !== ''){
                swal({
                    html        : '<pre>' + errormessage + '</pre>',
                    background  : 'transparent'
                });
            }else{
                save_data(url, form_data);
            }
            return false;
        });
    }
    function save_data(url, form_data){
        var text_info = '';
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    $.ajax({
                        type        : 'ajax',
                        method      : 'post',
                        url         : url,
                        data        : form_data,
                        async       : true,
                        processData : false,
                        contentType : false,
                        cache       : false,
                        dataType    : 'json',
                        success     : function(response){
                            if(response.success){
                                $('#modal_form_data').modal('hide');
                                $('#form_data')[0].reset();
                                if(response.type == 'saved'){
                                    text_info = 'disimpan';
                                }
                                if(response.type == 'updated'){
                                    text_info = 'diupdate';
                                }
                                swal({
                                    html                : '<pre>Data mata kuliah berhasil ' + text_info + '</pre>',
                                    type                : "success",
                                    allowOutsideClick   : false,
                                    allowEscapeKey      : false, 
                                    background          : 'transparent',
                                    showConfirmButton   : false,
                                    timer               : 1000
                                }).then(function(){
                                    $('#table_data').DataTable().ajax.reload();
                                });
                            }
                            if(response.match){
                                swal({
                                    background  : 'transparent',
                                    html        : '<pre>Kode mata kuliah sudah ada' + '<br>' +
                                                  'Harap gunakan kode lain</pre>',
                                    type        : "warning"
                                });
                            }
                        },
                        error       : function(){
                            swal({
                                background  : 'transparent',
                                html        : '<pre>Koneksi terputus' + '<br>' +
                                              'Cobalah beberapa saat lagi</pre>',
                                type        : "warning"
                            });
                        }
                    });
                }, 500);
            }
        });
    }
});