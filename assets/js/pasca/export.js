$(document).ready(function(){
    var specialElementHandlers = {
        "#editor"   : function(element, renderer){
            return true;
        }
    };
    var sum = (index, value) => index + value;
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
            if($.inArray(e, result) == -1){
                result.push(e);
            }
        });
        return result;
    }
    swal({
        showConfirmButton   : false,
        allowOutsideClick   : false,
        allowEscapeKey      : false,
        background          : 'transparent',
        onOpen  : function(){
            swal.showLoading();
            setTimeout(function(){
                transkrip_mahasiswa();
            },500);
        }
    });
    function transkrip_mahasiswa(){
        $.ajax({
            type        : 'ajax',
            method      : 'post',
            url         : site + 'pasca_log/transkrip_mahasiswa',
            data        : {id_transkrip : id_transkrip},
            async       : true,
            dataType    : 'json',
            success     : function(data){
                console.log(data);
                var table_data = 
                '<table id="table_data" style="width: 50%;">' +
                    '<tr>' +
                        '<td>NIM</td>' +
                        '<td>: ' + data.nim + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td>Nama</td>' +
                        '<td>: ' + data.nama + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td>Semester</td>' +
                        '<td>: ' + data.semester + '</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td>Jurusan</td>' +
                        '<td>: ' + data.jurusan + '</td>' +
                    '</tr>' +
                '</table>';
                $('#content_data').html(table_data);

                var table_transkrip = 
                '<table style="width: 100%;" border="1">' +
                    '<thead>' +
                        '<tr>' + 
                            '<th>No</th>' +
                            '<th>Kode Mata Kuliah</th>' +
                            '<th>Nama Mata Kuliah</th>' +
                            '<th>Nilai</th>' +
                        '</tr>' + 
                    '</thead>' +
                    '<tbody id="tbody_transkrip">' +
                        
                    '</tbody>' +
                '</table>';
                $('#content_transkrip').html(table_transkrip);
                var arr_matkul = data.matkul;
                var tbody_transkrip = '';
                for(i = 0; i < arr_matkul.length; i ++){
                    tbody_transkrip +=
                    '<tr>' + 
                        '<td>' + (i + 1) + '</td>' +
                        '<td>' + arr_matkul[i].kd + '</td>' +
                        '<td>' + arr_matkul[i].nama + '</td>' +
                        '<td>' + arr_matkul[i].nilai + '</td>' +
                    '</tr>';
                }
                $('#tbody_transkrip').html(tbody_transkrip);
                swal.close();
                var nm_mahasiswa = data.nama;
                print_pdf(nm_mahasiswa);
            },
            error       : function(){
                swal({
                    background  : 'transparent',
                    html        : '<pre>Koneksi terputus' + '<br>' +
                                  'Cobalah beberapa saat lagi</pre>',
                    type        : "warning"
                });
            }
        });
    }
    function print_pdf(nm_mahasiswa){
        var doc = new jsPDF();
        doc.fromHTML($('#target').html(),10,10,{
            'width'             : 500,
            'elementHandlers'   :specialElementHandlers
        });
        doc.save(nm_mahasiswa + '.pdf');
    }
});