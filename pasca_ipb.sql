-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2021 at 10:27 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pasca_ipb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE `tb_banner` (
  `id_banner` bigint(20) NOT NULL,
  `urutan` bigint(20) NOT NULL,
  `file` text NOT NULL,
  `dibuat` varchar(100) NOT NULL,
  `diupdate` varchar(100) NOT NULL,
  `update_by` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_banner`
--

INSERT INTO `tb_banner` (`id_banner`, `urutan`, `file`, `dibuat`, `diupdate`, `update_by`) VALUES
(1, 1, 'banner_ipb.png', '2021-01-02 16:24:20', '2021-01-03 16:37:47', 'admin1'),
(3, 2, 'pasca_ipb.JPG', '2021-01-03 17:01:32', '', 'admin1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_fakultas`
--

CREATE TABLE `tb_fakultas` (
  `id_fakultas` bigint(20) NOT NULL,
  `fakultas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_fakultas`
--

INSERT INTO `tb_fakultas` (`id_fakultas`, `fakultas`) VALUES
(1, 'Pertanian'),
(2, 'Kedokteran Hewan'),
(3, 'Perikanan dan Ilmu Kelautan'),
(4, 'Peternakan'),
(5, 'Kehutanan'),
(6, 'Teknologi Pertanian'),
(7, 'Matematika dan Ilmu Pengetahuan Alam'),
(8, 'Ekonomi dan Manajemen'),
(9, 'Ekologi Manusia');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` bigint(20) NOT NULL,
  `id_fakultas` bigint(20) NOT NULL,
  `jurusan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `id_fakultas`, `jurusan`) VALUES
(1, 1, 'Ilmu Tanah'),
(2, 1, 'Agroteknologi Tanah'),
(3, 1, 'Bioteknologi Tanah dan Lingkungan'),
(4, 1, 'Pengelolaan Daerah Aliran Sungai'),
(5, 1, 'Ilmu Perencanaan Wilayah'),
(6, 1, 'Ilmu dan Teknologi Benih'),
(7, 1, 'Agronomi dan Hortikultura'),
(8, 1, 'Pemuliaan dan Bioteknologi Tanaman'),
(9, 1, 'Entomologi'),
(10, 1, 'Fitopatologi'),
(11, 1, 'Arsitektur Lanskap'),
(12, 1, 'Pengelolaan Hama Terpadu'),
(13, 2, 'Ilmu-Ilmu Faal dan Khasiat Obat'),
(14, 2, 'Anatomi dan Perkembangan Hewan'),
(15, 2, 'Kesehatan Masyarakat Veteriner'),
(16, 2, 'Parasitologi dan Entomologi Kesehatan'),
(17, 2, 'Mikrobiologi Medik'),
(18, 2, 'Ilmu Biomedis Hewan'),
(19, 2, 'Biologi Reproduksi'),
(20, 3, 'Ilmu Akuakultur'),
(21, 3, 'Pengelolaan Sumberdaya Perairan'),
(22, 3, 'Pengelolaan Sumberdaya Pesisir dan Lautan'),
(23, 3, 'Teknologi Hasil Perairan'),
(24, 3, 'Teknologi Perikanan Laut'),
(25, 3, 'Ilmu Kelautan'),
(26, 3, 'Teknologi Kelautan'),
(27, 4, 'Ilmu Produksi dan Teknologi Peternakan'),
(28, 4, 'Ilmu Nutrisi dan Pakan'),
(29, 5, 'Ilmu Pengelolaan Hutan'),
(30, 5, 'Ilmu dan Teknologi Hasil Hutan'),
(31, 5, 'Konservasi Biodiversitas Tropika'),
(32, 5, 'Manajemen Ekowisata dan Jasa Lingkungan'),
(33, 5, 'Silvikultur Tropika'),
(34, 6, 'Teknik Mesin Pertanian dan Pangan'),
(35, 6, 'Teknologi Pascapanen'),
(36, 6, 'Teknik Sipil dan Lingkungan'),
(37, 6, 'Ilmu Pangan'),
(38, 6, 'Teknologi Industri Pertanian'),
(39, 7, 'Statistika'),
(40, 7, 'Statistika Terapan'),
(41, 7, 'Klimatologi Terapan'),
(42, 7, 'Mikrobiologi'),
(43, 7, 'Biosains Hewan'),
(44, 7, 'Biologi Tumbuhan'),
(45, 7, 'Kimia'),
(46, 7, 'Matematika Terapan'),
(47, 7, 'Ilmu Komputer'),
(48, 7, 'Biofisika'),
(49, 7, 'Biokimia'),
(50, 7, 'Master of Science in Information Technology for Natural Resources Management (Program Internasional)'),
(51, 8, 'Ilmu Ekonomi'),
(52, 8, 'Ilmu Manajemen'),
(53, 8, 'Ekonomi Sumberdaya dan Lingkungan'),
(54, 8, 'Ekonomi Sumberdaya Kelautan Tropika'),
(55, 8, 'Ilmu Ekonomi Pertanian'),
(56, 8, 'Agribisnis'),
(57, 8, 'Ilmu Perencanaan Pembangunan Wilayah dan Pedesaan'),
(58, 9, 'Ilmu Gizi'),
(59, 9, 'Ilmu Keluarga dan Perkembangan Anak'),
(60, 9, 'Ilmu Penyuluhan Pembangunan'),
(61, 9, 'Komunikasi Pembangunan Pertanian Pedesaan'),
(62, 9, 'Sosiologi Pedesaan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `id_mahasiswa` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `id_jurusan` bigint(20) NOT NULL,
  `nim` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `dibuat` varchar(50) NOT NULL,
  `diupdate` varchar(50) NOT NULL,
  `update_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_mata_kuliah`
--

CREATE TABLE `tb_mata_kuliah` (
  `id_mata_kuliah` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `id_jurusan` bigint(20) NOT NULL,
  `kd` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `dibuat` varchar(50) NOT NULL,
  `diupdate` varchar(50) NOT NULL,
  `update_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_semester`
--

CREATE TABLE `tb_semester` (
  `id_semester` bigint(20) NOT NULL,
  `semester` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_semester`
--

INSERT INTO `tb_semester` (`id_semester`, `semester`) VALUES
(1, 'Semester 1'),
(2, 'Semester 2'),
(3, 'Semester 3'),
(4, 'Semester 4'),
(5, 'Semester 5'),
(6, 'Semester 6'),
(7, 'Semester 7'),
(8, 'Semester 8');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transkrip`
--

CREATE TABLE `tb_transkrip` (
  `id_transkrip` bigint(20) NOT NULL,
  `id_mahasiswa` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `dibuat` varchar(50) NOT NULL,
  `diupdate` varchar(50) NOT NULL,
  `update_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transkrip_detail`
--

CREATE TABLE `tb_transkrip_detail` (
  `id_transkrip_detail` bigint(20) NOT NULL,
  `id_transkrip` bigint(20) NOT NULL,
  `id_mata_kuliah` bigint(20) NOT NULL,
  `nilai` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banner`
--
ALTER TABLE `tb_banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `tb_fakultas`
--
ALTER TABLE `tb_fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`id_mahasiswa`);

--
-- Indexes for table `tb_mata_kuliah`
--
ALTER TABLE `tb_mata_kuliah`
  ADD PRIMARY KEY (`id_mata_kuliah`);

--
-- Indexes for table `tb_semester`
--
ALTER TABLE `tb_semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `tb_transkrip`
--
ALTER TABLE `tb_transkrip`
  ADD PRIMARY KEY (`id_transkrip`);

--
-- Indexes for table `tb_transkrip_detail`
--
ALTER TABLE `tb_transkrip_detail`
  ADD PRIMARY KEY (`id_transkrip_detail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banner`
--
ALTER TABLE `tb_banner`
  MODIFY `id_banner` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_fakultas`
--
ALTER TABLE `tb_fakultas`
  MODIFY `id_fakultas` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  MODIFY `id_mahasiswa` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_mata_kuliah`
--
ALTER TABLE `tb_mata_kuliah`
  MODIFY `id_mata_kuliah` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_semester`
--
ALTER TABLE `tb_semester`
  MODIFY `id_semester` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_transkrip`
--
ALTER TABLE `tb_transkrip`
  MODIFY `id_transkrip` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_transkrip_detail`
--
ALTER TABLE `tb_transkrip_detail`
  MODIFY `id_transkrip_detail` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
