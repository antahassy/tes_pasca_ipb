<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pasca_log extends CI_Model{

    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function all_banner(){
        $this->db->order_by('urutan', 'asc');
        $this->db->select('file');
        $query = $this->db->get('tb_banner');
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function o_semester(){
        $this->db->order_by('id_semester', 'asc');
        $query = $this->db->get('tb_semester');
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function o_jurusan(){
        $this->db->order_by('id_jurusan', 'asc');
        $query = $this->db->get('tb_jurusan');
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

    public function autocomplete_nim($nim){
		$this->db->select('nim');
		$this->db->like('nim', $nim, 'both');
		return $this->db->get('tb_mahasiswa')->result();
    }

    public function nim_complete($nim){
          $this->db->select('nim, nama');
          $query = $this->db->get_where('tb_mahasiswa', array(
               'nim' => $nim
          ));
          if($query->num_rows() > 0){
               return $query->row();
          }else{
               return false;
          }
    }

    public function autocomplete_nama($nama){
		$this->db->select('nama');
		$this->db->like('nama', $nama, 'both');
		return $this->db->get('tb_mahasiswa')->result();
    }

    public function nama_complete($nama){
          $this->db->select('nim, nama');
          $query = $this->db->get_where('tb_mahasiswa', array(
               'nama' => $nama
          ));
          if($query->num_rows() > 0){
               return $query->row();
          }else{
               return false;
          }
    }
    //start mata kuliah
    public function datatable_mata_kuliah(){
        $this->db->select('
            tb_mata_kuliah.id_mata_kuliah,
            tb_mata_kuliah.kd,
            tb_mata_kuliah.nama,
            tb_mata_kuliah.dibuat,
            tb_mata_kuliah.diupdate,
            tb_mata_kuliah.update_by,
            tb_semester.semester,
            tb_jurusan.jurusan
        ');
        $column_order = array(null, 
            'tb_mata_kuliah.id_mata_kuliah',
            'tb_mata_kuliah.kd',
            'tb_mata_kuliah.nama',
            'tb_mata_kuliah.dibuat',
            'tb_mata_kuliah.diupdate',
            'tb_mata_kuliah.update_by',
            'tb_semester.semester',
            'tb_jurusan.jurusan'
        );
        $column_search = array(
            'tb_mata_kuliah.id_mata_kuliah',
            'tb_mata_kuliah.kd',
            'tb_mata_kuliah.nama',
            'tb_mata_kuliah.dibuat',
            'tb_mata_kuliah.diupdate',
            'tb_mata_kuliah.update_by',
            'tb_semester.semester',
            'tb_jurusan.jurusan'
        );
        $i = 0;
        $this->db->from('tb_mata_kuliah');
        $this->db->join('tb_semester', 'tb_mata_kuliah.id_semester = tb_semester.id_semester', 'left');
        $this->db->join('tb_jurusan', 'tb_mata_kuliah.id_jurusan = tb_jurusan.id_jurusan', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_mata_kuliah.id_mata_kuliah', 'desc');
        }
    }
 
    public function table_mata_kuliah(){
        $this->datatable_mata_kuliah();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function total_table_mata_kuliah(){
        $this->db->from('tb_mata_kuliah');
        return $this->db->count_all_results();
    }
 
    public function total_filter_table_mata_kuliah(){
        $this->datatable_mata_kuliah();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function check_kd_matkul($kd_matkul){
    	$this->db->select('id_mata_kuliah');
		$query = $this->db->get_where('tb_mata_kuliah', array('kd' => $kd_matkul));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function check_update_kd_matkul($id_mata_kuliah, $kd_matkul){
    	$this->db->select('id_mata_kuliah');
		$query = $this->db->get_where('tb_mata_kuliah', array(
			'id_mata_kuliah !=' 	=> $id_mata_kuliah,
			'kd' 					=> $kd_matkul,
		));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function save_matkul(){
		$data = array(
			'id_semester'		=> $this->input->post('s_semester'),
			'id_jurusan'		=> $this->input->post('s_jurusan'),
			'kd'				=> $this->input->post('kd_matkul'),
			'nama'				=> $this->input->post('nm_matkul'),
			'dibuat'			=> date('Y-m-d H:i:s'),
			'update_by'			=> $this->input->post('form_ip_address')
		);
		$query = $this->db->insert('tb_mata_kuliah', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
	}

	public function edit_matkul($id_mata_kuliah){
		$this->db->where('id_mata_kuliah', $id_mata_kuliah);
        $query = $this->db->get('tb_mata_kuliah');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
	}

	public function update_matkul($id_mata_kuliah){
		$data = array(
			'id_semester'		=> $this->input->post('s_semester'),
			'id_jurusan'		=> $this->input->post('s_jurusan'),
			'kd'				=> $this->input->post('kd_matkul'),
			'nama'				=> $this->input->post('nm_matkul'),
			'diupdate'			=> date('Y-m-d H:i:s'),
			'update_by'			=> $this->input->post('form_ip_address')
		);
		$this->db->where('id_mata_kuliah', $id_mata_kuliah);
        $this->db->update('tb_mata_kuliah', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
	}

	public function delete_matkul($id_mata_kuliah){
		$this->db->where('id_mata_kuliah', $id_mata_kuliah); 
        $this->db->delete('tb_mata_kuliah');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false; 
        }
	}
    //end mata kuliah
    //start mahasiswa
    public function datatable_mahasiswa(){
        $this->db->select('
            tb_mahasiswa.id_mahasiswa,
            tb_mahasiswa.nim,
            tb_mahasiswa.nama,
            tb_mahasiswa.dibuat,
            tb_mahasiswa.diupdate,
            tb_mahasiswa.update_by,
            tb_semester.semester,
            tb_jurusan.jurusan
        ');
        $column_order = array(null, 
            'tb_mahasiswa.id_mahasiswa',
            'tb_mahasiswa.nim',
            'tb_mahasiswa.nama',
            'tb_mahasiswa.dibuat',
            'tb_mahasiswa.diupdate',
            'tb_mahasiswa.update_by',
            'tb_semester.semester',
            'tb_jurusan.jurusan'
        );
        $column_search = array(
            'tb_mahasiswa.id_mahasiswa',
            'tb_mahasiswa.nim',
            'tb_mahasiswa.nama',
            'tb_mahasiswa.dibuat',
            'tb_mahasiswa.diupdate',
            'tb_mahasiswa.update_by',
            'tb_semester.semester',
            'tb_jurusan.jurusan'
        );
        $i = 0;
        $this->db->from('tb_mahasiswa');
        $this->db->join('tb_semester', 'tb_mahasiswa.id_semester = tb_semester.id_semester', 'left');
        $this->db->join('tb_jurusan', 'tb_mahasiswa.id_jurusan = tb_jurusan.id_jurusan', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_mahasiswa.id_mahasiswa', 'desc');
        }
    }
 
    public function table_mahasiswa(){
        $this->datatable_mahasiswa();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function total_table_mahasiswa(){
        $this->db->from('tb_mahasiswa');
        return $this->db->count_all_results();
    }
 
    public function total_filter_table_mahasiswa(){
        $this->datatable_mahasiswa();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function check_nim_mhs($nim_mahasiswa){
    	$this->db->select('id_mahasiswa');
		$query = $this->db->get_where('tb_mahasiswa', array('nim' => $nim_mahasiswa));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function check_update_nim_mhs($id_mahasiswa, $nim_mahasiswa){
    	$this->db->select('id_mahasiswa');
		$query = $this->db->get_where('tb_mahasiswa', array(
			'id_mahasiswa !=' 		=> $id_mahasiswa,
			'nim' 					=> $nim_mahasiswa,
		));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function check_available_mhs($nim, $nama){
    	$this->db->select('id_mahasiswa');
		$query = $this->db->get_where('tb_mahasiswa', array(
			'nim'	=> $nim,
			'nama' 	=> $nama,
		));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function save_mhs(){
		$data = array(
			'id_semester'		=> $this->input->post('s_semester'),
			'id_jurusan'		=> $this->input->post('s_jurusan'),
			'nim'				=> $this->input->post('nim_mahasiswa'),
			'nama'				=> $this->input->post('nm_mahasiswa'),
			'dibuat'			=> date('Y-m-d H:i:s'),
			'update_by'			=> $this->input->post('form_ip_address')
		);
		$query = $this->db->insert('tb_mahasiswa', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
	}

	public function edit_mhs($id_mahasiswa){
		$this->db->where('id_mahasiswa', $id_mahasiswa);
        $query = $this->db->get('tb_mahasiswa');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
	}

	public function update_mhs($id_mahasiswa){
		$data = array(
			'id_semester'		=> $this->input->post('s_semester'),
			'id_jurusan'		=> $this->input->post('s_jurusan'),
			'nim'				=> $this->input->post('nim_mahasiswa'),
			'nama'				=> $this->input->post('nm_mahasiswa'),
			'diupdate'			=> date('Y-m-d H:i:s'),
			'update_by'			=> $this->input->post('form_ip_address')
		);
		$this->db->where('id_mahasiswa', $id_mahasiswa);
        $this->db->update('tb_mahasiswa', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
	}

	public function delete_mhs($id_mahasiswa){
		$this->db->where('id_mahasiswa', $id_mahasiswa); 
        $this->db->delete('tb_mahasiswa');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false; 
        }
	}
    //end mahasiswa
    //start transkrip
    public function datatable_transkrip(){
        $this->db->select('
            tb_transkrip.id_transkrip,
            tb_transkrip.id_semester,
            tb_transkrip.dibuat,
            tb_transkrip.diupdate,
            tb_transkrip.update_by,
            tb_mahasiswa.nim,
            tb_mahasiswa.nama,
            tb_mahasiswa.id_jurusan,
            tb_semester.semester
        ');
        $column_order = array(null, 
            'tb_transkrip.id_transkrip',
            'tb_transkrip.id_semester',
            'tb_transkrip.dibuat',
            'tb_transkrip.diupdate',
            'tb_transkrip.update_by',
            'tb_mahasiswa.nim',
            'tb_mahasiswa.nama',
            'tb_mahasiswa.id_jurusan',
            'tb_semester.semester'
        );
        $column_search = array(
            'tb_transkrip.id_transkrip',
            'tb_transkrip.id_semester',
            'tb_transkrip.dibuat',
            'tb_transkrip.diupdate',
            'tb_transkrip.update_by',
            'tb_mahasiswa.nim',
            'tb_mahasiswa.nama',
            'tb_mahasiswa.id_jurusan',
            'tb_semester.semester'
        );
        $i = 0;
        $this->db->from('tb_transkrip');
        $this->db->join('tb_mahasiswa', 'tb_transkrip.id_mahasiswa = tb_mahasiswa.id_mahasiswa', 'left');
        $this->db->join('tb_semester', 'tb_transkrip.id_semester = tb_semester.id_semester', 'left');
        foreach ($column_search as $item){
            if($_POST['search']['value'] != ''){
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i){
                    $this->db->group_end();
                } 
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
            $this->db->order_by('tb_transkrip.id_transkrip', 'desc');
        }
    }
 
    public function table_transkrip(){
        $this->datatable_transkrip();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function total_table_transkrip(){
        $this->db->from('tb_transkrip');
        return $this->db->count_all_results();
    }
 
    public function total_filter_table_transkrip(){
        $this->datatable_transkrip();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function check_transkrip($id_mahasiswa, $id_semester){
    	$this->db->select('id_transkrip');
		$query = $this->db->get_where('tb_transkrip', array(
			'id_mahasiswa'	=> $id_mahasiswa,
			'id_semester' 	=> $id_semester,
		));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
    }

    public function save_transkrip($id_mahasiswa, $id_semester){
		$data = array(
			'id_mahasiswa'		=> $id_mahasiswa,
			'id_semester'		=> $id_semester,
			'dibuat'			=> date('Y-m-d H:i:s'),
			'update_by'			=> $this->input->post('form_ip_address')
		);
		$query = $this->db->insert('tb_transkrip', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
	}

    public function transkrip_mhs_detail($id_transkrip){
        $this->db->order_by('tb_transkrip_detail.id_transkrip_detail', 'asc');
        $this->db->select('
            tb_transkrip_detail.nilai, 
            tb_mata_kuliah.kd, 
            tb_mata_kuliah.nama
        ');
        $this->db->where('tb_transkrip_detail.id_transkrip', $id_transkrip);
        $this->db->from('tb_transkrip_detail');
        $this->db->join('tb_mata_kuliah', 'tb_transkrip_detail.id_mata_kuliah = tb_mata_kuliah.id_mata_kuliah', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{ 
            return false;
        }
    }

	public function transkrip_detail($id_transkrip){
		$this->db->select('id_transkrip_detail');
		$query = $this->db->get_where('tb_transkrip_detail', array(
			'id_transkrip'	=> $id_transkrip
		));
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

    public function transkrip_mhs($id_transkrip){
        $this->db->select('
            tb_mahasiswa.nim, 
            tb_mahasiswa.nama, 
            tb_semester.semester, 
            tb_jurusan.jurusan
        ');
        $this->db->where('id_transkrip', $id_transkrip);
        $this->db->from('tb_transkrip');
        $this->db->join('tb_mahasiswa', 'tb_transkrip.id_mahasiswa = tb_mahasiswa.id_mahasiswa', 'left');
        $this->db->join('tb_jurusan', 'tb_mahasiswa.id_jurusan = tb_jurusan.id_jurusan', 'left');
        $this->db->join('tb_semester', 'tb_transkrip.id_semester = tb_semester.id_semester', 'left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row();
        }else{ 
            return false;
        }
    }

    public function transkrip_matkul($id_jurusan, $id_semester){
        $this->db->select('id_mata_kuliah, kd, nama');
        $query = $this->db->get_where('tb_mata_kuliah', array(
            'id_jurusan' => $id_jurusan,
            'id_semester' => $id_semester
        ));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function transkrip_nilai($id_transkrip, $id_mata_kuliah){
        $this->db->select('id_transkrip_detail, id_mata_kuliah, nilai');
        $query = $this->db->get_where('tb_transkrip_detail', array(
            'id_transkrip' => $id_transkrip,
            'id_mata_kuliah' => $id_mata_kuliah
        ));
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    public function save_transkrip_nilai($id_transkrip, $id_mata_kuliah, $nilai){
        $data = array(
            'id_transkrip'      => $id_transkrip,
            'id_mata_kuliah'    => $id_mata_kuliah,
            'nilai'             => $nilai
        );
        $query = $this->db->insert('tb_transkrip_detail', $data);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function update_transkrip_nilai($id_transkrip_detail, $nilai){
        $data = array('nilai'   => $nilai);
        $this->db->where('id_transkrip_detail', $id_transkrip_detail);
        $this->db->update('tb_transkrip_detail', $data);
        if($this->db->affected_rows() >= 0){
            return true;
        }else{
            return false;
        }
    }
    //end transkrip
}