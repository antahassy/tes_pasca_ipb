<style>
    #table_transkrip tr th, #table_transkrip tr td{
        padding: 0 2.5px;
    }
    #table_transkrip_nilai tr th, #table_transkrip_nilai tr td{
        padding: 5px;
        font-size: 14px;
        text-align: left;
    }
</style>
<div class="content"> 
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full" style="min-height: 410px;">
            <h2 style="text-align: center;"><?php echo $active_menu ?></h2>
            <button type="button" class="btn btn-outline-primary" id="btn_add">Transkrip Baru</button>
            <div class="row" style="margin-top: 15px;">
            	<div class="col-md-12">
                    <table id="table_data" border="1" style="width: 100%;">
                    	<thead>
                    		<tr>
	                    		<th>No</th>
                                <th>Tindakan</th>
	                    		<th>NIM</th>
                                <th>Nama</th>
	                    		<th>Semester</th>
	                    		<th>Dibuat</th>
                                <!-- th>Diupdate</th> -->
	                    		<th>Oleh</th>
	                    	</tr>
                    	</thead>
                    </table>
            	</div>
            </div>
        </div> 
    </div> 
</div>
<div class="modal animated" id="modal_form_data" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_data">
                    <input type="hidden" name="form_ip_address" value="" id="form_ip_address">
                    <input type="hidden" name="id_transkrip" value="" id="id_transkrip" class="form-control">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>NIM Mahasiswa</label>
                            <input type="text" name="nim_mahasiswa" id="nim_mahasiswa" placeholder="Autocomplete" class="form-control">
                            <label>Nama Mahasiswa</label>
                            <input type="text" name="nm_mahasiswa" id="nm_mahasiswa" placeholder="Autocomplete" class="form-control">
                            <label>Semester</label>
                            <select name="s_semester" id="s_semester" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_save"></button>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form_nilai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_nilai">
                    <input type="hidden" name="form_nilai_ip_address" value="" id="form_nilai_ip_address">
                    <input type="hidden" name="id_transkrip" value="" id="id_transkrip" class="form-control">
                    <div class="form-group">
                        <div class="col-md-12">
                            <table id="table_transkrip">
                                <tr>
                                    <td>NIM</td>
                                    <td id="transkrip_nim"></td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td id="transkrip_nama"></td>
                                </tr>
                                <tr>
                                    <td>Semester</td>
                                    <td id="transkrip_semester"></td>
                                </tr>
                                <tr>
                                    <td>Jurusan</td>
                                    <td id="transkrip_jurusan"></td>
                                </tr>
                            </table>
                            <table id="table_transkrip_nilai" style="width: 100%; margin-top: 25px;" border="1">
                                <thead>
                                    <tr>
                                        <th>Kode Mata Kuliah</th>
                                        <th>Nama Mata Kuliah</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_transkrip_nilai"></tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_save_nilai"></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/js/pasca/transkrip_nilai.js?t=').mt_rand()?>"></script>