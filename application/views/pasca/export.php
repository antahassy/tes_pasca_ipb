
<!DOCTYPE html>
<!--Antahassy Wibawa-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title><?php echo $judul_web ?></title>
        <meta name="description" content="E-Multi Marketing">
        <meta name="author" content="Antahassy Wibawa">
        <meta name="robots" content="noindex, nofollow">
        
        <meta property="og:title" content="<?php echo $judul_web ?>">
        <meta property="og:site_name" content="<?php echo $judul_web ?>">
        <meta property="og:description" content="<?php echo $judul_web ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <link rel="shortcut icon" href="<?php echo site_url('assets/image/base_logo.png?t=').mt_rand()?>">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo site_url('assets/image/base_logo.png?t=').mt_rand()?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url('assets/image/base_logo.png?t=').mt_rand()?>">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo site_url('assets/session/css/dashmix.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" id="css-theme" href="<?php echo site_url('assets/session/css/themes/xmodern.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/antahassy.css?t=').mt_rand()?>">

        <link rel="stylesheet" href="<?php echo site_url('assets/session/js/plugins/datatables/dataTables.bootstrap4.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/session/js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/jquery-ui.min.css?t=').mt_rand()?>">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/sweetalert2.min.css?t=').mt_rand()?>">

        <script src="<?php echo site_url('assets/session/js/dashmix.core.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/dashmix.app.min.js?t=').mt_rand()?>"></script>

        <script src="<?php echo site_url('assets/session/js/plugins/datatables/jquery.dataTables.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/dataTables.bootstrap4.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/buttons/dataTables.buttons.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/buttons/buttons.print.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/buttons/buttons.html5.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/buttons/buttons.flash.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/plugins/datatables/buttons/buttons.colVis.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/session/js/pages/be_tables_datatables.min.js?t=').mt_rand()?>"></script>

        <script src="<?php echo site_url('assets/js/jquery-ui.min.js?t=').mt_rand()?>"></script>
        <script src="<?php echo site_url('assets/js/sweetalert2.all.min.js?t=').mt_rand()?>"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.1.135/jspdf.min.js"></script> -->
        <!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.2.0/jspdf.umd.min.js"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    </head>
    <script type="text/javascript">
        var site = '<?php echo site_url()?>';
        var ip_address = '';
        var id_transkrip = '<?php echo $id_transkrip?>';
        $.get('https://jsonip.com', function(address){
            ip_address = address.ip;
        });
    </script>
    <style>
        body, body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){
            overflow-y: auto !important;
            padding-right: 0 !important;
        }
        .swal2-popup{
            width: 30em !important;
            padding: inherit !important;
            border-radius: 0.5em !important;
        }
        .swal2-popup .swal2-footer{
            margin: 0 !important;
            padding: 0 !important;
            border-top: none !important;
        }
        .swal2-icon{
            margin: 5px !important;
        }
        .swal2-actions{
            margin: 5px !important;
        }
        .swal2-actions button{
            font-weight: 600 !important;
        }
        .swal2-container.swal2-shown {
            background-color: rgba(0,0,0,0.75);
        }
        .swal2-popup .swal2-content{
            max-height: 200px !important;
            overflow-y: auto !important;
        }
        .swal2-popup .swal2-styled:focus{
            outline: 0;
            box-shadow: none !important;
        }
        .swal2-popup .swal2-styled.swal2-confirm{
            background-color: blue !important;
        }
        .swal2-popup .swal2-confirm:hover, .swal2-confirm:focus{
            background-color: rgb(0,0,100) !important;
        }
        .swal2-popup .swal2-styled.swal2-cancel{
            background-color: red !important;
        }
        .swal2-popup .swal2-cancel:hover, .swal2-cancel:focus{
            background-color: rgb(100,0,0) !important;
        }
        .swal2-image{
            height: 100px;
            width: 100px;
            border-radius: 100%;
        }
        pre{
            font-family: verdana !important; 
            font-size: 14px !important;
            color: #fff;
        }
        .ui-datepicker{
            z-index: 99999 !important;
        }
        #ui-datepicker-div{
            width: auto;
            left: calc(50% - 300px / 2) !important;
            font-size: 15px !important;
        }
        .ui-autocomplete{
            z-index: 99999 !important;
            max-height: 300px !important;
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }
        .name_color{
            color: #fff !important;
        }
        .name_color:hover{
            color: orange !important;
        }
        #page-header .content-header{
            padding-left: 0.75rem;
            padding-right: 0.75rem;
        }
        .content-header{
            height: 3.375rem;
        }
        .simplebar-content-wrapper, #page-container.page-header-glass.page-header-fixed.page-header-scroll.page-header-dark #page-header{
            background-color: transparent;
        }
        .nav-main-dark .nav-main-link, .page-header-dark #page-header .nav-main-link, .sidebar-dark #sidebar .nav-main-link{
            color: #fff;
        }
        .nav-main-dark .nav-main-link, .page-header-dark #page-header .nav-main-link, .sidebar-dark #sidebar .nav-main-link:hover{
            color: orange !important;
        }
        .dataTables_filter{
            text-align: right;
        }
        .bg-image{
            background-repeat: round;
            background-color: #fff;
            border-bottom: 3px solid orange;
            height: 152.5px;
            top: 0;
            position: sticky;
            z-index: 10;
        }
        table tr th{
            background-color: #35383e;
            color: #fff;
        }
        #table_data tr th, #table_data tr td{
            padding: 2.5px;
        }
        table tr th, table tr td{
            padding: 7.5px;
        }
    </style>
    <body id="target">
        <div style="width: 100%; padding: 25px;">
            <div id="content_data"></div>
            <div id="content_transkrip" style="margin-top: 15px;"></div>
        </div>
    </body>
    <script type="text/javascript" src="<?php echo site_url('assets/js/pasca/export.js?t=').mt_rand()?>"></script>
</html>