<div class="content"> 
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full" style="min-height: 410px;">
            <h2 style="text-align: center;"><?php echo $active_menu ?></h2>
            <button type="button" class="btn btn-outline-primary" id="btn_add">Tambah Mata Kuliah</button>
            <div class="row" style="margin-top: 15px;">
            	<div class="col-md-12">
                    <table id="table_data" border="1" style="width: 100%;">
                    	<thead>
                    		<tr>
	                    		<th>No</th>
                                <th>Tindakan</th>
	                    		<th>Kode Matkul</th>
	                    		<th>Nama Matkul</th>
                                <th>Semester</th>
	                    		<th>Jurusan</th>
	                    		<th>Dibuat</th>
	                    		<th>Diupdate</th>
	                    		<th>Oleh</th>
	                    	</tr>
                    	</thead>
                    </table>
            	</div>
            </div>
        </div> 
    </div> 
</div>
<div class="modal animated" id="modal_form_data" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_data">
                    <input type="hidden" name="form_ip_address" value="" id="form_ip_address">
                    <input type="hidden" name="id_mata_kuliah" value="" id="id_mata_kuliah" class="form-control">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Kode Mata Kuliah</label>
                            <input type="text" name="kd_matkul" id="kd_matkul" class="form-control">
                            <label>Nama Mata Kuliah</label>
                            <input type="text" name="nm_matkul" id="nm_matkul" class="form-control">
                            <label>Semester</label>
                            <select name="s_semester" id="s_semester" class="form-control">
                                <option value=""></option>
                            </select>
                            <label>Jurusan</label>
                            <select name="s_jurusan" id="s_jurusan" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_save"></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/js/pasca/mata_kuliah.js?t=').mt_rand()?>"></script>