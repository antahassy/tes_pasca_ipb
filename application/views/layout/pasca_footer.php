        </main>
        <footer id="page-footer" class="bg-body-light">
            <div class="content py-0">
                <div class="row font-size-sm">
                    <div class="col-sm-12 order-sm-1 text-center text-sm-center">
                        Copyright © 2021 by <?php echo $creator_web ?>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>    