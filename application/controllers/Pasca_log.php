<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasca_log extends CI_Controller {

	public function __construct(){ 
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('M_pasca_log', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
    }

    public function all_banner(){
        $data = $this->model->all_banner();
        echo json_encode($data);
    }

    public function o_semester(){
        $data = $this->model->o_semester();
        echo json_encode($data);
    }

    public function o_jurusan(){
        $data = $this->model->o_jurusan();
        echo json_encode($data);
    }

    public function autocomplete_nim(){
        if(isset($_GET['term'])){
            $result = $this->model->autocomplete_nim($_GET['term']); 
            if(count($result) > 0){
                foreach ($result as $row){
                    $all_result[] = $row->nim;
                }
            echo json_encode($all_result);
            }
        }
    }

    public function nim_complete(){
        $nim = $this->input->post('nim');
        $data = $this->model->nim_complete($nim);
        echo json_encode($data);
    }

    public function autocomplete_nama(){
        if(isset($_GET['term'])){
            $result = $this->model->autocomplete_nama($_GET['term']); 
            if(count($result) > 0){
                foreach ($result as $row){
                    $all_result[] = $row->nama;
                }
            echo json_encode($all_result);
            }
        }
    }

    public function nama_complete(){
        $nama = $this->input->post('nama');
        $data = $this->model->nama_complete($nama);
        echo json_encode($data);
    }
    //start mata kuliah
    public function table_mata_kuliah(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->table_mata_kuliah();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = '<button type="button" class="btn btn-sm btn-outline-primary btn_edit" style="margin: 2.5px;" id="' . $field->id_mata_kuliah . '">Edit</button>' . '<br>' . '<button type="button" class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;" id="' . $field->id_mata_kuliah . '" data="' . $field->nama . '">Hapus</button>';
            $row[] = $field->kd;
            $row[] = $field->nama;
            $row[] = $field->semester;
            $row[] = $field->jurusan;

            if($field->dibuat != ''){
                $split_dibuat = explode(' ', $field->dibuat);
                $key_dibuat = explode('-', $split_dibuat[0]);
                $row[] = $key_dibuat[2] .'/'. $month_format[(int)$key_dibuat[1]] .'/'. $key_dibuat[0] . '<br>' . $split_dibuat[1];
            }else{
                $row[] = '';
            }

            if($field->diupdate != ''){
                $split_diupdate = explode(' ', $field->diupdate);
                $key_diupdate = explode('-', $split_diupdate[0]);
                $row[] = $key_diupdate[2] .'/'. $month_format[(int)$key_diupdate[1]] .'/'. $key_diupdate[0] . '<br>' . $split_diupdate[1];
            }else{
                $row[] = '';
            }

            $row[] = $field->update_by;
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_table_mata_kuliah(),
            "recordsFiltered" => $this->model->total_filter_table_mata_kuliah(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save_matkul(){
        $message['type'] = 'saved';
        $kd_matkul = $this->input->post('kd_matkul');
        $check_kd_matkul = $this->model->check_kd_matkul($kd_matkul);
        if($check_kd_matkul){
            $message['match'] = true;
            echo json_encode($message);
        }else{
            $data = $this->model->save_matkul();
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function edit_matkul(){
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $data = $this->model->edit_matkul($id_mata_kuliah); 
        echo json_encode($data);
    }

    public function update_matkul(){
        $message['type'] = 'updated';
        $kd_matkul = $this->input->post('kd_matkul');
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $check_update_kd_matkul = $this->model->check_update_kd_matkul($id_mata_kuliah, $kd_matkul);
        if($check_update_kd_matkul){
            $message['match'] = true;
            echo json_encode($message);
        }else{
            $data = $this->model->update_matkul($id_mata_kuliah);
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function delete_matkul(){
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $data = $this->model->delete_matkul($id_mata_kuliah);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
    //end mata kuliah
    //start mahasiswa
    public function table_mahasiswa(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->table_mahasiswa();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = '<button type="button" class="btn btn-sm btn-outline-primary btn_edit" style="margin: 2.5px;" id="' . $field->id_mahasiswa . '">Edit</button>' . '<br>' . '<button type="button" class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;" id="' . $field->id_mahasiswa . '" data="' . $field->nama . '">Hapus</button>';
            $row[] = $field->nim;
            $row[] = $field->nama;
            $row[] = $field->semester;
            $row[] = $field->jurusan;

            if($field->dibuat != ''){
                $split_dibuat = explode(' ', $field->dibuat);
                $key_dibuat = explode('-', $split_dibuat[0]);
                $row[] = $key_dibuat[2] .'/'. $month_format[(int)$key_dibuat[1]] .'/'. $key_dibuat[0] . '<br>' . $split_dibuat[1];
            }else{
                $row[] = '';
            }

            if($field->diupdate != ''){
                $split_diupdate = explode(' ', $field->diupdate);
                $key_diupdate = explode('-', $split_diupdate[0]);
                $row[] = $key_diupdate[2] .'/'. $month_format[(int)$key_diupdate[1]] .'/'. $key_diupdate[0] . '<br>' . $split_diupdate[1];
            }else{
                $row[] = '';
            }

            $row[] = $field->update_by;
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_table_mahasiswa(),
            "recordsFiltered" => $this->model->total_filter_table_mahasiswa(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save_mhs(){
        $message['type'] = 'saved';
        $nim_mahasiswa = $this->input->post('nim_mahasiswa');
        $check_nim_mhs = $this->model->check_nim_mhs($nim_mahasiswa);
        if($check_nim_mhs){
            $message['match'] = true;
            echo json_encode($message);
        }else{
            $data = $this->model->save_mhs();
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function edit_mhs(){
        $id_mahasiswa = $this->input->post('id_mahasiswa');
        $data = $this->model->edit_mhs($id_mahasiswa); 
        echo json_encode($data);
    }

    public function update_mhs(){
        $message['type'] = 'updated';
        $nim_mahasiswa = $this->input->post('nim_mahasiswa');
        $id_mahasiswa = $this->input->post('id_mahasiswa');
        $check_update_nim_mhs = $this->model->check_update_nim_mhs($id_mahasiswa, $nim_mahasiswa);
        if($check_update_nim_mhs){
            $message['match'] = true;
            echo json_encode($message);
        }else{
            $data = $this->model->update_mhs($id_mahasiswa);
            if($data){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function delete_mhs(){
        $id_mahasiswa = $this->input->post('id_mahasiswa');
        $data = $this->model->delete_mhs($id_mahasiswa);
        if($data){
            $message['success'] = true;
            echo json_encode($message);
        }
    }
    //end mahasiswa
    //start transkrip
    public function table_transkrip(){
        $month_format = array (1 => 
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $list = $this->model->table_transkrip();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();

            $row[] = $no;

            $transkrip_detail = $this->model->transkrip_detail($field->id_transkrip);
            if($transkrip_detail){
                $row[] = '<button type="button" class="btn btn-sm btn-outline-primary btn_edit" style="margin: 2.5px;" id="' . $field->id_transkrip . '" data="' . $field->id_semester . '" alt="' . $field->id_jurusan . '">Edit Nilai</button>' . '<br>' . '<a href="' . site_url('pasca/export/' . $field->id_transkrip) . '" target="_blank" class="btn btn-sm btn-outline-danger btn_export" style="margin: 2.5px;">Export</a>';
            }else{
                $row[] = '<button type="button" class="btn btn-sm btn-outline-primary btn_edit" style="margin: 2.5px;" id="' . $field->id_transkrip . '" data="' . $field->id_semester . '" alt="' . $field->id_jurusan . '">Tambah Nilai</button>';
            }
            $row[] = $field->nim;
            $row[] = $field->nama;
            $row[] = $field->semester;

            if($field->dibuat != ''){
                $split_dibuat = explode(' ', $field->dibuat);
                $key_dibuat = explode('-', $split_dibuat[0]);
                $row[] = $key_dibuat[2] .'/'. $month_format[(int)$key_dibuat[1]] .'/'. $key_dibuat[0] . '<br>' . $split_dibuat[1];
            }else{
                $row[] = '';
            }

            // if($field->diupdate != ''){
            //     $split_diupdate = explode(' ', $field->diupdate);
            //     $key_diupdate = explode('-', $split_diupdate[0]);
            //     $row[] = $key_diupdate[2] .'/'. $month_format[(int)$key_diupdate[1]] .'/'. $key_diupdate[0] . '<br>' . $split_diupdate[1];
            // }else{
            //     $row[] = '';
            // }

            $row[] = $field->update_by;
 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->total_table_transkrip(),
            "recordsFiltered" => $this->model->total_filter_table_transkrip(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function save_transkrip(){
        $message['type'] = 'saved';
        $nim = $this->input->post('nim_mahasiswa');
        $nama = $this->input->post('nm_mahasiswa');
        $check_available_mhs = $this->model->check_available_mhs($nim, $nama);
        if($check_available_mhs){
            $id_mahasiswa = $check_available_mhs->id_mahasiswa;
            $id_semester = $this->input->post('s_semester');
            $check_transkrip = $this->model->check_transkrip($id_mahasiswa, $id_semester);
            if($check_transkrip){
                $message['match'] = true;
                echo json_encode($message);
            }else{
                $data = $this->model->save_transkrip($id_mahasiswa, $id_semester);
                if($data){
                    $message['success'] = true;
                    echo json_encode($message);
                }
            }
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function transkrip_data(){
        $id_jurusan = $this->input->post('id_jurusan');
        $id_semester = $this->input->post('id_semester');
        $transkrip_matkul = $this->model->transkrip_matkul($id_jurusan, $id_semester);
        if($transkrip_matkul){
            $id_transkrip = $this->input->post('id_transkrip');
            $transkrip_mhs = $this->model->transkrip_mhs($id_transkrip); 
            $object = array(
                'nim'       => $transkrip_mhs->nim,
                'nama'      => $transkrip_mhs->nama,
                'semester'  => $transkrip_mhs->semester,
                'jurusan'   => $transkrip_mhs->jurusan,
                'matkul'    => $transkrip_matkul
            );
            echo json_encode($object);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function transkrip_nilai(){
        $id_transkrip = $this->input->post('id_transkrip');
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $data = $this->model->transkrip_nilai($id_transkrip, $id_mata_kuliah);
        if($data){
            echo json_encode($data);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }

    public function save_transkrip_nilai(){
        $id_transkrip = $this->input->post('id_transkrip');
        $id_mata_kuliah = $this->input->post('id_mata_kuliah');
        $nilai = $this->input->post('nilai');
        $check = $this->model->transkrip_nilai($id_transkrip, $id_mata_kuliah);
        if($check){
            $id_transkrip_detail = $check->id_transkrip_detail;
            $updated = $this->model->update_transkrip_nilai($id_transkrip_detail, $nilai);
            if($updated){
                $message['success'] = true;
                echo json_encode($message);
            }
        }else{
            $saved = $this->model->save_transkrip_nilai($id_transkrip, $id_mata_kuliah, $nilai);
            if($saved){
                $message['success'] = true;
                echo json_encode($message);
            }
        }
    }

    public function transkrip_mahasiswa(){
        $id_transkrip = $this->input->post('id_transkrip');
        $transkrip_mhs = $this->model->transkrip_mhs($id_transkrip); 
        if($transkrip_mhs){
            $transkrip_mhs_detail = $this->model->transkrip_mhs_detail($id_transkrip);
            $object = array(
                'nim'       => $transkrip_mhs->nim,
                'nama'      => $transkrip_mhs->nama,
                'semester'  => $transkrip_mhs->semester,
                'jurusan'   => $transkrip_mhs->jurusan,
                'matkul'    => $transkrip_mhs_detail
            );
            echo json_encode($object);
        }else{
            $message['empty'] = true;
            echo json_encode($message);
        }
    }
    //end transkrip
}