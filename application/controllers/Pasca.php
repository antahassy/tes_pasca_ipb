<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasca extends CI_Controller {

	public function __construct(){ 
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('M_pasca', 'model');
        require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
    }

	public function index(){
        $data['judul_web'] = 'Pascasarjana IPB - Dashboard';
        $data['creator_web'] = 'Antahassy Wibawa';
        $data['active_menu'] = 'Dashboard';
        $jam=date('H');
        if($jam >= 11 && $jam < 15){
            $data['waktu'] = "Siang";
        }
        if($jam >= 15 && $jam < 18){
            $data['waktu'] = "Sore";
        }
        if($jam >= 18 && ($jam < 24 || $jam < 00)){
            $data['waktu'] = "Malam";
        }
        if(($jam >= 24 || $jam >= 00) && $jam <= 10){
            $data['waktu'] = "Pagi";
        }

        $this->load->view('layout/pasca_header', $data);
        $this->load->view('pasca/index');
        $this->load->view('layout/pasca_footer');
	}

    public function mata_kuliah(){
        $data['judul_web'] = 'Pascasarjana IPB - Mata Kuliah';
        $data['creator_web'] = 'Antahassy Wibawa';
        $data['active_menu'] = 'Mata Kuliah';
        $this->load->view('layout/pasca_header', $data);
        $this->load->view('pasca/mata_kuliah');
        $this->load->view('layout/pasca_footer');
    }

    public function mahasiswa(){
        $data['judul_web'] = 'Pascasarjana IPB - Mahasiswa';
        $data['creator_web'] = 'Antahassy Wibawa';
        $data['active_menu'] = 'Mahasiswa';
        $this->load->view('layout/pasca_header', $data);
        $this->load->view('pasca/mahasiswa');
        $this->load->view('layout/pasca_footer');
    }

    public function transkrip_nilai(){
        $data['judul_web'] = 'Pascasarjana IPB - Transkrip Nilai';
        $data['creator_web'] = 'Antahassy Wibawa';
        $data['active_menu'] = 'Transkrip Nilai';
        $this->load->view('layout/pasca_header', $data);
        $this->load->view('pasca/transkrip_nilai');
        $this->load->view('layout/pasca_footer');
    }

    public function export($id){
        $data['judul_web'] = 'Pascasarjana IPB - Export PDF';
        $data['id_transkrip'] = $id;
        $this->load->view('pasca/export', $data);
    }
}
